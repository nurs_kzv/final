const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviews = new Schema({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
  },
  restaurant: {
    type: Schema.Types.ObjectId,
    ref: 'Restaurants',
    required: true,
  },
  review: {
    type: String,
    required: true,
  },
  food: {
    type: Number,
    required: true,
  },
  service: {
    type: Number,
    required: true,
  },
  interior: {
    type: Number,
    required: true,
  }
});

const Reviews = mongoose.model('Reviews', reviews);
module.exports = Reviews;