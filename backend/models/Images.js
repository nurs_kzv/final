const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const images = new Schema({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
  },
  restaurant: {
    type: Schema.Types.ObjectId,
    ref: 'Restaurants',
    required: true,
  },
  photo: {
    type: String,
    required: true,
  }
});

const Images = mongoose.model('Images', images);
module.exports = Images;