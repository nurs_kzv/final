const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const restaurants = new Schema({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  reviews: [
    {
      author: Schema.Types.ObjectId,
      review: String,
      food: Number,
      service: Number,
      interior:  Number,
      date: String,
    }
  ]
});

restaurants.methods.currentTime = function() {
  this.reviews.date = new Date();
};

const Restaurants = mongoose.model('Restaurants', restaurants);
module.exports = Restaurants;

