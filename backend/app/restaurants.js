const express = require('express');
const Restaurants = require('../models/Restaurants');
const {nanoid} = require("nanoid");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const path = require("path");
const multer = require('multer');
const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
  destination:(req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename:(req, file, cb) => {
    cb(null, nanoid(4) + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const {author, title, description} = req.body;
    const image = req?.file ? '/uploads/' + req.file.filename : null;
    const newPlace = new Restaurants({
      author,
      title,
      description,
      image,
    });
    await newPlace.save();
    res.status(200).send(newPlace);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/', async (req, res) => {
  try {
    const restaurants = await Restaurants.find();
    res.status(200).send(restaurants);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get('/review/:id', async (req, res) => {
  try {
    const data = await Restaurants.findById(req.params.id);
    res.status(200).send(data);
  } catch (e) {
    res.status(500).send(e);
  }
})

router.post('/review/:id', async (req, res) => {
  try {
    const {author, review, food, service, interior} = req.body;
    const date = new Date();
    const cafe = await Restaurants.findById(req.params.id);
    cafe.reviews.push({author, review, food, service, interior, date});
    await cafe.save();
    res.sendStatus(200);
  } catch (e) {
    res.status(400).send(e);
  }
})

router.delete('/:id',auth, permit('admin'), async (req, res) => {
  try {
    await Restaurants.findByIdAndDelete(req.params.id);
    res.sendStatus(200);
  } catch (e) {
    res.status(400).send(e);
  }
})

module.exports = router;