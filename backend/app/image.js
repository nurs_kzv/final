const express = require('express');
const auth = require("../middleware/auth");
const Images = require("../models/Images");
const path = require("path");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const router = express.Router();

const storage = multer.diskStorage({
  destination:(req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename:(req, file, cb) => {
    cb(null, nanoid(4) + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const {author, restaurant} = req.body;
    const photo = req?.file ? '/uploads/' + req.file.filename : null;
    const newImage = await Images.create({
      author,
      restaurant,
      photo
    });
    res.sendStatus(200);
    await newImage.save();
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const images = await Images.find({restaurant: req.params.id});
    res.status(200).send(images);
  } catch (e) {
    res.status(500).send(e);
  }
})

module.exports = router;