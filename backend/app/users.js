const express = require('express');
const Users = require('../models/Users');
const config = require('../config');
const axios = require("axios");
const {nanoid} = require("nanoid");
const router = express.Router();

router.post('/',  async (req, res) => {
  try {
    const {email, password, username, avatar} = req.body;
    const user = new Users({
      email,
      password,
      username,
      avatar,
    });

    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
  const user = await Users.findOne({email: req.body.email});

  if (!user) {
    return res.status(401).send({message: 'User not found'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: 'Credentials are wrong'});
  }

  user.generateToken();
  await user.save();
  return res.send(user);
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const user = await Users.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save();

  return res.send(success);
});


router.get('/:id', async (req, res) => {
  try {
    const users = await Users.findById(req.params.id);
    res.status(200).send(users);
  } catch (e) {
    res.status(400)
  }
})

module.exports = router;