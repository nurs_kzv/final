const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const Users = require("./models/Users");
const Restaurants = require("./models/Restaurants");
const Images = require("./models/Images");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, Jack, Jone] = await Users.create({
    email: 'user@mail.com',
    password: '123',
    token: nanoid(),
    username: 'user',
  }, {
    email: 'jack@test.com',
    password: 'jack123',
    token: nanoid(),
    username: 'Jack',
  }, {
    email: 'jone@test.com',
    password: 'jone123',
    token: nanoid(),
    username: 'jone',
  }, {
    email: 'admin@mail.ru',
    password: '123',
    token: nanoid(),
    username: 'admin',
    role: 'admin',
  });

  const [pearl, ghost, rice] = await Restaurants.create({
    author: Jack,
    title: 'Black Pearl',
    description: 'Welcome to our restaurant!',
    image: '/fixtures/pearl.jpeg',
    reviews: [
      {
        author: Jone,
        review: 'Lovely place',
        food: 5,
        service: 5,
        interior: 5,
        date: new Date(),
      }
    ]
  }, {
    author: Jone,
    title: 'Ghost',
    description: 'Welcome to our restaurant!',
    image: '/fixtures/ghost.png',
    reviews: [
      {
        author: Jack,
        review: 'Lovely place',
        food: 5,
        service: 5,
        interior: 5,
        date: new Date(),
      }
    ]
  }, {
    author: user,
    title: 'Rice rest',
    description: 'Rice restaurant est. 1899',
    image: '/fixtures/rice.jpg',
    reviews: [
      {
        author: Jack,
        review: 'Lovely place',
        food: 5,
        service: 5,
        interior: 5,
        date: new Date(),
      }
    ]
  });


  await Images.create({
    author: user,
    restaurant: ghost,
    photo: '/fixtures/rice.jpg'
  }, {
    author: Jack,
    restaurant: rice,
    photo: '/fixtures/pearl.jpeg',
  }, {
    author: Jone,
    restaurant: pearl,
    photo: '/fixtures/ghost.png'
  })

  await mongoose.connection.close();
};

run().catch(e => console.error(e));