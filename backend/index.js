require('dotenv').config();
const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./app/users');
const restaurants = require('./app/restaurants');
const image = require('./app/image');
const app = express();
app.use(express.static('./public'));
app.use(express.json());
app.use(cors());

app.use('/users', users);
app.use('/restaurant', restaurants);
app.use('/image', image);

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  app.listen(config.port, () => {
    console.log(`Server started on ${config.port} port!`);
  });

  exitHook(async callback => {
    await mongoose.disconnect();
    console.log('mongoose disconnected');
    callback();
  });
}

run().catch(console.error);
