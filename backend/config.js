const path = require('path');
const rootPath = __dirname;

const env = process.env.NODE_ENV;

let databaseUrl = 'mongodb://localhost/critic-api';
let port = 8000;

if ( env === 'test') {
  databaseUrl = 'mongodb://localhost/exam_test';
  port = 8010;
}

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  port,
  db: {
    url: databaseUrl,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    },
  }
};