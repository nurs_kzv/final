const { I } = inject();


Given('я нахожусь на странице логина', () => {
  I.amOnPage('/login');
});

When('я ввожу в поля текст:', table => {
  for (const id in table.rows) {
    if (id < 1) {
      continue;
    }
    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

When('нажимаю {string}', (name) => {
  I.click('//button//*')
});

Given('я нахожусь на странице добавления места', () => {
  I.amOnPage('/add');
});


// И('нажимаю кнопку', () => {
//   // From "features/addPlace.feature" {"line":18,"column":5}
//   throw new Error('Not implemented yet');
// });
//
// И('добавляю изображение {string}', () => {
//   // From "features/addPlace.feature" {"line":19,"column":5}
//   throw new Error('Not implemented yet');
// });
//
// И('нажимаю чекбокс', () => {
//   // From "features/addPlace.feature" {"line":20,"column":5}
//   throw new Error('Not implemented yet');
// });
//
// То('вижу {string}', () => {
//   // From "features/addPlace.feature" {"line":22,"column":5}
//   throw new Error('Not implemented yet');
// });
