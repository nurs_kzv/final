import React, {useState} from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/FormElement/FormElement";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {postReviewRequest} from "../../store/sagas/cafeSaga";

const useStyles = makeStyles(({
  root: {
    width: '90%',
    margin: '10px auto',
    textAlign: "center",
  },
  input: {
    display: 'none',
  },
  photoBlock: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '60%',
    margin:'10px auto',
    flexDirection: "column"
  },
  formElement: {
    display: 'block',
    margin: 10,
    width: '100%',
  },
  fileBlock: {
    flexWrap: 'nowrap',
    alignItems: "center",
    marginBottom: 20
  },
  photoBtn: {
    margin: 5,
  },
  selects: {
    display: 'flex',
    alignItems: 'space-between'
  }
}))

const rate = [
  {name: 1, _id: 1},
  {name: 2, _id: 2},
  {name: 3, _id: 3},
  {name: 4, _id: 4},
  {name: 5, _id: 5},
]

const AddReview = ({id}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state?.users.user);
  const [newPost, setNewPost] = useState({author: user._id, review: '', food:5, service:5, interior:5});

  const onchangeHandler = e => {
    const {name, value} = e.target;

    setNewPost( prevState => ({
      ...prevState,
      [name]:value,
    }));
  };

  const createHandler = () => {
    dispatch(postReviewRequest([id, newPost]));
  };

  return (
    <Grid container>
      <Grid item className={classes.root}>

        <Typography variant='h4'>
          Review
        </Typography>


          <Grid item className={classes.formElement}>
            <FormElement
              required
              name='review'
              variant='outlined'
              fullWidth
              value={newPost.review}
              multiline
              rows={5}
              label='review'
              onChange={onchangeHandler}
            />

            <Grid className={classes.selects}>
              <FormElement
                onChange={(e)=>onchangeHandler(e)}
                label='Food'
                name='food'
                select={true}
                options={rate}
                required
                value={newPost.food}
              />

              <FormElement
                onChange={(e)=>onchangeHandler(e)}
                label='Service'
                name='service'
                select={true}
                options={rate}
                required
                value={newPost.service}
              />

              <FormElement
                onChange={(e)=>onchangeHandler(e)}
                label='Interior'
                name='interior'
                select={true}
                options={rate}
                required
                value={newPost.interior}/>
            </Grid>

          </Grid>

          <Button
            color='primary'
            onClick={createHandler}
            variant='outlined'>
            add
          </Button>
      </Grid>
    </Grid>
  );
};

export default AddReview;