import React, {useState} from 'react';
import {Grid, makeStyles, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import {useDispatch, useSelector} from "react-redux";
import {postImageRequest} from "../../store/sagas/imageSaga";

const useStyles = makeStyles({
  fileBlock: {
    flexWrap: 'nowrap',
    alignItems: "center",
    marginBottom: 20
  },
  photoBtn: {
    margin: 5,
  },
  input: {
    display: 'none',
  }
})

const AddImage = ({id}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state?.users.user._id);
  const [postImage, setPostImage] = useState('');

  const fileChangeHandler = e => {
    if(e.target.files[0].name){
      const file = e.target.files[0];
      setPostImage(file);
    } else {
      setPostImage('');
    }
  };

  const createHandler = () => {
    const body = {
      author: user,
      restaurant: id,
      image: postImage,
    };

    const formData = new FormData();

    Object.keys(body).map(key => (
      formData.append(key, body[key])
    ));
    dispatch(postImageRequest([formData, id]));
  };

  return (
    <Grid container item direction={'column'} className={classes.fileBlock}>
      <TextField
        disabled
        required
        fullWidth
        label='Add image'
        variant='outlined'
        value={postImage.name ? postImage.name : ''}
      />
      <input
        accept="image/*"
        required
        className={classes.input}
        id="contained-button-file"
        onChange={fileChangeHandler}
        multiple
        type="file"
      />
      <label htmlFor="contained-button-file">
        <Button
          variant="contained"
          color="primary"
          component="span"
          endIcon={<AddAPhotoIcon/>}
          className={classes.photoBtn}>
          Upload
        </Button>
      </label>
      <Button
        color='primary'
        onClick={createHandler}
        variant='outlined'>
        add
      </Button>
    </Grid>
  );
};

export default AddImage;