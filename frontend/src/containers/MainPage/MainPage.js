import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getCafesRequest} from "../../store/sagas/cafeSaga";
import {Grid} from "@material-ui/core";
import CafeCard from "../../components/CafeCard/CafeCard";

const MainPage = () => {
  const dispatch = useDispatch();
  const cafe = useSelector(state => state?.cafes.cafes);

  useEffect(() => {
    dispatch(getCafesRequest());
  },[dispatch]);

  return (
    <Grid container>
      {cafe.map(obj => (
        <CafeCard
          key={obj._id}
          id={obj._id}
          title={obj.title}
          description={obj.description}
          image={obj.image}
          reviews={obj.reviews}
        />
      ))}
    </Grid>
  );
};

export default MainPage;