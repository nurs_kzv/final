import React, {useEffect} from 'react';
import {Grid, makeStyles} from "@material-ui/core";
import PlaceReview from "../../components/PlaceReview/PlaceReview";
import {getOneCafeRequest} from "../../store/sagas/cafeSaga";
import {useDispatch, useSelector} from "react-redux";
import Rating from "../../components/Rating/Rating";
import Review from "../../components/Review/Review";
import AddReview from "../AddReview/AddReview";
import AddImage from "../AddImage/AddImage";
import ShowImages from "../../components/ShowImages/ShowImages";

const useStyles = makeStyles(({
  itemBlock: {
    width: '95%',
  }

}))

const CafeReview = (props) => {
  const id = props.match.params.id;
  const dispatch = useDispatch();
  const classes = useStyles();
  const place = useSelector(state => state?.cafes.onePlace);

  useEffect(() => {
    dispatch(getOneCafeRequest(id));
  }, [dispatch, id]);

  return (
    <Grid container justifyContent={'center'} direction={'column'}>
      <Grid item className={classes.itemBlock}>
        <PlaceReview
          title={place.title}
          description={place.description}
          image={place.image}
        />
      </Grid>

      <Grid item className={classes.itemBlock}>
        <ShowImages id={id}/>
      </Grid>

      <Grid item className={classes.itemBlock}>
        <Rating
          reviews={place.reviews}
        />
      </Grid>
      <Grid item className={classes.itemBlock}>
        {place?.reviews?.map(obj => (
          <Review
            key={obj._id}
            author={obj.author}
            date={obj.date}
            review={obj.review}
            food={obj.food}
            service={obj.service}
            interior={obj.interior}
          />
        ))}
      </Grid>
      <Grid item className={classes.itemBlock}>
        <AddReview id={id}/>
      </Grid>
      <Grid item className={classes.itemBlock}>
        <AddImage id={id}/>
      </Grid>
    </Grid>
  );
};

export default CafeReview;