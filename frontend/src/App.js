import React from "react";
import './App.css';
import {Route, Switch, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import CustomAppBar from "./components/UI/CustomAppBar/CustomAppBar";
import MainPage from "./containers/MainPage/MainPage";
import AddPlace from "./containers/AddPlace/AddPlace";
import CafeReview from "./containers/CafeReview/CafeReview";

function App() {
  const user = useSelector(state => state.users.user);
  const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
      <Route {...props}/> :
      <Redirect to={redirectTo}/>
  };
  return (
    <>
      <CustomAppBar/>
      <Switch>
        <Route path='/' exact component={MainPage}/>
        <Route path='/register' exact component={Register}/>
        <Route path='/login' exact component={Login}/>
        <ProtectedRoute
          path='/add'
          component={AddPlace}
          isAllowed={user._id}
          redirectTo='/login'
        />
        <Route path='/review/:id' component={CafeReview}/>
      </Switch>
    </>
  );
}

export default App;