import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import {FormControlLabel, withStyles} from "@material-ui/core";

const CheckBox = withStyles({
  root: {
    color: '#b0bec5',
    height: '25px',
    '&$checked': {
      color: '#37474f',
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const CustomCheckBox = ({checked, children, setState}) => {

  const handleChange = event => {
    setState(event.target.checked);
  };

  return (
    <FormControlLabel
      control={<CheckBox checked={checked} onChange={handleChange} name="checked"/>}
      label={children}
    />
  );
}

export default CustomCheckBox;