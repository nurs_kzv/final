import React from 'react';
import {useDispatch} from "react-redux";
import Button from "@material-ui/core/Button";
import {Menu, MenuItem} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {historyPush} from "../../../store/sagas/historySaga";
import {logoutRequest} from "../../../store/sagas/userSaga";
import {Link} from "react-router-dom";

const useStyle = makeStyles({
  header: {
    color: 'white',
    fontWeight:'bold'
  },
  avatar: {
    width: 30,
    height: 30,
    border: '1px solid white',
    borderRadius: '50%',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  }
});

const LoggedInUser = ({username}) => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logOutHandler =  () => {
    dispatch(logoutRequest());
    dispatch(historyPush('/'));
  };

  return (
    <>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        className={classes.header}
      >
        Hello, {username}
      </Button>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={logOutHandler}> Log Out </MenuItem>
        <MenuItem component={Link} to={'/add'}> Add new place </MenuItem>
      </Menu>
    </>
  );
};

export default LoggedInUser;