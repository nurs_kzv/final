import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {apiUrl} from "../../config";
import {Link} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {useSelector} from "react-redux";
import axiosApi from "../../axiosApi";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    display: 'block',
    width: 'auto',
    margin: '10px auto',
    textAlign: 'center',
    paddingBottom: 10
  },
  media: {
    height: 140,
    cursor: 'pointer'
  },
});

const CafeCard = ({image, title, reviews, description, id}) => {
  const classes = useStyles();
  let food = 0;
  let service = 0;
  let interior = 0;
  let total = 0;
  const length = reviews.length;

  if (length > 0) {
    for (let i=0; i<length; i++){
      food += reviews[i].food;
      service += reviews[i].service;
      interior += reviews[i].interior;
    }
    total = ((food/length) + (service/length) + (interior/length))/3;
  }

  const user = useSelector(state => state?.users.user);

  const deleteHandler = async (id) => {
    await axiosApi.delete(`/restaurant/${id}`)
  }
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image = {apiUrl + image}
          title = {title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            <Link href={`/review/${id}`}>
              {title}
            </Link>
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {description}
          </Typography>
          <Typography variant="body2"  component="p">
            (Score:{Math.ceil(total)} {reviews.length} reviews)
          </Typography>
        </CardContent>
      </CardActionArea>
      {user.role === 'admin' && (
        <Button onClick={() => deleteHandler(id)}>
          delete
        </Button>
      )}
    </Card>
  );
}

export default CafeCard;