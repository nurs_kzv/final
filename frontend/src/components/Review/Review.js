import React, {useEffect} from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getUserRequest} from "../../store/sagas/userSaga";

const useStyles = makeStyles(({
  root: {
    display: 'flex',
    width: '90%',
    margin: '10px auto',
    border: '1px solid black'
  }
}))

const Review = ({date, author, review, food, service, interior}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const name = useSelector(state => state?.users.oneUser);
  useEffect(() => {
    dispatch(getUserRequest(author));
  }, [dispatch, author])

  return (
    <Grid container direction={'column'} className={classes.root}>
      <Typography variant={'h5'}>
        On {date}, {name.username} said:
      </Typography>
      <Typography variant={'h5'}>
        {review}
      </Typography>
      <Typography variant={'h6'}>
        Food: {food}
      </Typography>
      <Typography variant={'h6'}>
        Service: {service}
      </Typography>
      <Typography variant={'h6'}>
        Interior: {interior}
      </Typography>
    </Grid>
  );
};

export default Review;