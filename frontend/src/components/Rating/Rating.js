import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles(({
  root: {
    display: 'flex',
    width: '90%',
    margin: '10px auto',
  }
}))

const Rating = ({reviews}) => {
  const classes = useStyles();
  let food = 0;
  let service = 0;
  let interior = 0;
  let total = 0;
  const length = reviews?.length;

  if (length > 0) {
    for (let i=0; i<length; i++){
      food += reviews[i].food;
      service += reviews[i].service;
      interior += reviews[i].interior;
    }
    total = ((food/length) + (service/length) + (interior/length))/3;
  }

  return (
    <Grid container direction={'column'} className={classes.root}>
      <Typography variant={'h3'}>
        Rating
      </Typography>
      <Typography variant={'h4'}>
        Overall: {Math.floor(total)}
      </Typography>
      <Typography variant={'h4'}>
        Food: {Math.ceil(food/length)}
      </Typography>
      <Typography variant={'h4'}>
        Service: {Math.round(service/length)}
      </Typography>
      <Typography variant={'h4'}>
        Interior: {Math.round(service/length)}
      </Typography>
    </Grid>
  );
};

export default Rating;