import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {apiUrl} from "../../config";
import CardMedia from "@material-ui/core/CardMedia";
const useStyles = makeStyles({
  root: {
    display: 'flex',
    width: '90%',
    margin: '10px auto',
    alignItems: 'center'
  },
  text: {
    textAlign: 'center',
    width: '50%'
  },
  photo: {
    width: '50%',
  },
  photoBlock: {
    width : '200px',
    height: 'auto',
    margin: '10px'
  },
  title: {
    margin: '20px 0'
  },
  media: {
    height: 140,
    cursor: 'pointer',
    margin: "auto",
  },
})

const PlaceReview = ({title, description, image}) => {
  const classes = useStyles();
  return (
    <Grid item className={classes.root}>
      <Grid className={classes.text}>
        <Typography variant={'h2'} className={classes.title}>
          {title}
        </Typography>
        <Typography variant={'h4'}>
          {description}
        </Typography>

      </Grid>
      <Grid className={classes.photo}>
        <CardMedia
          className={classes.media}
          image = {apiUrl + image}
          title = {title}
        />
      </Grid>
    </Grid>
  );
};

export default PlaceReview;