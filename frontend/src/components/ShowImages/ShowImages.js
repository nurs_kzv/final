import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getImageRequest} from "../../store/sagas/imageSaga";
import {Grid, makeStyles} from "@material-ui/core";
import {apiUrl} from "../../config";
import CardMedia from "@material-ui/core/CardMedia";

const useStyles = makeStyles({
  media: {
    width: 70,
    height: 70,
    cursor: 'pointer',
    margin: "auto",
  }
})

const ShowImages = ({id}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const images = useSelector(state => state.images.images);

  useEffect(() => {
    dispatch(getImageRequest(id));
  },[dispatch, id]);

  return (
    <Grid container justifyContent={'space-evenly'}>
      {images.map(obj => (
        <CardMedia
          key={obj._id}
          className={classes.media}
          image = {apiUrl + obj.photo}
        />
      ))}
    </Grid>
  );
};

export default ShowImages;