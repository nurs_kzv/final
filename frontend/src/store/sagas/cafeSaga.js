import cafeSlices from "../slices/cafeSlices";
import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import axiosApi from "../../axiosApi";


export const {
  getCafesRequest,
  getCafeSuccess,
  getCafeFailure,
  postCafeRequest,
  postCafeSuccess,
  postCafeFailure,
  getOneCafeRequest,
  getOneCafeSuccess,
  getOneCafeFailure,
  postReviewRequest,
  postReviewSuccess,
  postReviewFailure,
} = cafeSlices.actions

export function* getCafe() {
  try {
    const response = yield axiosApi.get('/restaurant');
    yield put(getCafeSuccess(response.data));
  } catch (e) {
    if (e.response.data) {
      yield put(getCafeFailure(e.response.data));
      NotificationManager.error(e.response.data);
    } else {
      yield put(getCafeFailure(e));
      NotificationManager.error('Something went wrong. Check connection.');
    }
  }
}

export function* postCafe({payload: data}) {
  try {
    yield axiosApi.post('/restaurant', data);
    const response = yield axiosApi.get('/restaurant');
    yield put(postCafeSuccess(response.data));
    NotificationManager.success('New place added successfully');
  } catch (e) {
    if (e.response.data) {
      yield put(postCafeFailure(e.response.data));
      NotificationManager.error(e.response.data._message);
    } else {
      yield put(postCafeFailure(e));
      NotificationManager.error(e);
    }
  }
}

export function* getOneCafe({payload: id}) {
  try {
    const response = yield axiosApi.get(`/restaurant/review/${id}`);
    yield put(getOneCafeSuccess(response.data));
  } catch (e) {
    if (e.response.data) {
      yield put(getOneCafeFailure(e.response.data));
      NotificationManager.error(e.response.data._message);
    } else {
      yield put(getOneCafeFailure(e));
      NotificationManager.error(e);
    }
  }
}

export function* postReview({payload: [id, data]}) {
  try {
    yield axiosApi.post(`/restaurant/review/${id}`, data);
    const response = yield axiosApi.get(`/restaurant/review/${id}`)
    yield put(postReviewSuccess(response.data));
  } catch (e) {
    if (e.response.data) {
      yield put(postReviewFailure(e.response.data));
      NotificationManager.error(e.response.data._message);
    } else {
      yield put(postReviewFailure(e));
      NotificationManager.error(e);
    }
  }
}

const cafeSaga = [
  takeEvery(getCafesRequest, getCafe),
  takeEvery(postCafeRequest, postCafe),
  takeEvery(getOneCafeRequest, getOneCafe),
  takeEvery(postReviewRequest, postReview),
];

export default cafeSaga;