import {put, takeEvery} from 'redux-saga/effects';
import {NotificationManager} from "react-notifications";
import userSlices from "../slices/userSlices";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historySaga";

export const {
  loginFailure,
  loginRequest,
  loginSuccess,
  logoutRequest,
  logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess,
  getUserRequest,
  getUserSuccess,
  getUserFailure,
} = userSlices.actions;

export function* registerUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users',userData);
    yield put(registerSuccess(response.data));
    yield put(historyPush('/'));
    NotificationManager.success('Success')
  } catch (error) {
    yield put(registerFailure(error.response.data));
    NotificationManager.error(error.message);
  }
}

export function* loginUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data));
    yield put(historyPush('/'));
    NotificationManager.success('Success');
  } catch (error) {
    yield put(loginFailure(error.response.data));
    NotificationManager.error(error.message);
  }
}

export function* logout() {
  try {
    yield axiosApi.delete('/users/sessions');
    yield put(logoutSuccess());
    yield put(historyPush('/'));
  } catch (e) {

  }
}

export function* getUser({payload: id}) {
  try {
    const response = yield axiosApi.get(`/users/${id}`);
    yield put(getUserSuccess(response.data));
  } catch (e) {
    yield put(getUserFailure(e))
  }
}

const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(logoutRequest, logout),
  takeEvery(getUserRequest, getUser),
];

export default usersSagas;