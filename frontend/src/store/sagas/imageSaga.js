import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import imageSlices from "../slices/imageSlices";
import axiosApi from "../../axiosApi";

export const {
  getImageRequest,
  getImagesFailure,
  getImageSuccess,
  postImageRequest,
  postImageSuccess,
  postImageFailure,
} = imageSlices.actions

export function* getImage({payload: id}) {
  try {
    const response = yield axiosApi.get(`/image/${id}`);
    yield put(getImageSuccess(response.data));
  } catch (e) {
    if (e.response.data) {
      yield put(getImagesFailure(e.response.data));
      NotificationManager.error(e.response.data._message);
    } else {
      yield put(getImagesFailure(e));
      NotificationManager.error(e);
    }
  }
}

export function* postImage({payload: [data, id]}) {
  try {
    yield axiosApi.post('/image', data);
    const response = yield axiosApi.get(`/image/${id}`);
    yield put(postImageSuccess(response.data))
  } catch (e) {
    if (e.response.data) {
      yield put(postImageFailure(e.response.data));
      NotificationManager.error(e.response.data._message);
    } else {
      yield put(postImageFailure(e));
      NotificationManager.error(e);
    }
  }
}

 const imageSaga = [
   takeEvery(getImageRequest, getImage),
   takeEvery(postImageRequest, postImage),
]

export default imageSaga;