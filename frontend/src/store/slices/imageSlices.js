import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  loading: false,
  images: [],
  error: null,
}

const name = 'images';

const imageSlices = createSlice({
  name,
  initialState,
  reducers: {
    getImageRequest: state => {
      state.loading = true;
    },
    getImageSuccess: (state, {payload: data}) => {
      state.loading = false;
      state.images = data;
    },
    getImagesFailure: (state, {payload: error}) => {
      state.loading = false;
      state.error = error;
    },
    postImageRequest: state => {
      state.loading = false;
    },
    postImageSuccess: (state, {payload: data}) => {
      state.loading = false;
      state.images = data;
    },
    postImageFailure: (state, {payload: error}) => {
      state.loading = false;
      state.error = error;
    }
  }
})

export default imageSlices;