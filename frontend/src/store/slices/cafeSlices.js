import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  loading: false,
  cafes: [],
  error: null,
  onePlace: {},
};

const name = 'cafe';

const cafeSlices = createSlice({
  name,
  initialState,
  reducers: {
    getCafesRequest: state => {
      state.loading = true;
    },
    getCafeSuccess: (state, {payload: data}) => {
      state.cafes = data;
      state.loading = false;
      state.error = null;
    },
    getCafeFailure: (state, {payload: error}) => {
      state.error = error;
      state.loading = false;
    },
    postCafeRequest: state => {
      state.loading = false;
    },
    postCafeSuccess: (state, {payload: data}) => {
      state.loading = false;
      state.error = null;
    },
    postCafeFailure: (state, {payload: error}) => {
      state.error = error;
    },
    getOneCafeRequest: state => {
      state.loading = true;
    },
    getOneCafeSuccess: (state, {payload: data}) => {
      state.onePlace = data;
      state.loading = false;
      state.error = null;
    },
    getOneCafeFailure: (state, {payload: error}) => {
      state.error = error;
      state.loading = false;
    },
    postReviewRequest: state => {
      state.loading = true;
    },
    postReviewSuccess: (state, {payload: data}) => {
      state.loading = false;
      state.onePlace = data;
    },
    postReviewFailure: (state, {payload: error}) => {
      state.loading = false;
      state.error = error;
    }
  }
});

export default cafeSlices;