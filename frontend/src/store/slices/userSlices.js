import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null,
  user: {},
  oneUser: {},
};

const name = 'users';

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerRequest: state => {
      state.registerLoading = true;
    },
    registerSuccess: (state, {payload: user}) => {
      state.registerLoading = false;
      state.registerError = null;
      state.user = user;
    },
    registerFailure: (state, {payload: error}) => {
      state.registerLoading = false;
      state.registerError = error;
    },
    loginRequest: state => {
      state.loginLoading = true;
    },
    loginSuccess: (state, {payload: user}) => {
      state.loginLoading = false;
      state.loginError = null;
      state.user = user;
    },
    loginFailure: (state, {payload: error}) => {
      state.loginLoading = false;
      state.loginError = error;
    },
    logoutRequest: () => {},
    logoutSuccess: state => {
      state.user = {};
    },
    getUserRequest: state => {
      state.loading = true
    },
    getUserSuccess: (state, {payload: data}) => {
      state.loading = false;
      state.oneUser = data;
    },
    getUserFailure: (state, {payload: error}) => {
      state.loading = false;
      state.error = error;
    }
  }
});

export default usersSlice;