import {combineReducers} from "redux";
import usersSlice from "./slices/userSlices";
import cafeSlices from "./slices/cafeSlices";
import imageSlices from "./slices/imageSlices";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  cafes: cafeSlices.reducer,
  images: imageSlices.reducer,
});

export default rootReducer