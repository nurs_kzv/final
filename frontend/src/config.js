const env = process.env.REACT_APP_ENV;
let domain = 'localhost:8000';

if (env === 'test') {
  domain = 'localhost:8010';
}

export const apiUrl = 'http://' + domain;